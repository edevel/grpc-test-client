import com.google.protobuf.gradle.*

plugins {
    application
    id("com.google.protobuf") version "0.8.10"
}

repositories {
    jcenter()
}

val grpc_ver = "1.24.0"

dependencies {
    implementation("com.google.protobuf:protobuf-java:3.6.1")
    implementation("io.grpc:grpc-stub:${grpc_ver}")
    implementation("io.grpc:grpc-protobuf:${grpc_ver}")
    runtimeOnly("io.grpc:grpc-okhttp:${grpc_ver}")

    testImplementation("junit:junit:4.12")
}

application {
    // Define the main class for the application
    mainClassName = "gRPC.test.client.App"
}

protobuf {
    protoc {
        // The artifact spec for the Protobuf Compiler
        artifact = "com.google.protobuf:protoc:3.6.1"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:${grpc_ver}"
        }
    }

    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                id("grpc")
            }
        }
    }
}

sourceSets {
    main {
        java {
            srcDirs("build/generated/source/proto/main/grpc")
            srcDirs("build/generated/source/proto/main/java")
        }
    }
}